package com.hna.sae.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sae_wallet_history")
public class WalletHistory extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Column(name = "amount", nullable = false)
	private Double amount;

	@Enumerated(EnumType.STRING)
	@Column(name = "token_type", nullable = false)
	private WalletOperation walletOperation;

	public enum WalletOperation {
		ADD, DEDUCT
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public WalletOperation getWalletOperation() {
		return walletOperation;
	}

	public void setWalletOperation(WalletOperation walletOperation) {
		this.walletOperation = walletOperation;
	}

}

package com.hna.sae.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sae_status_work_flow")
public class StatusWorkFlow extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Status.class)
	@JoinColumn(name = "from_status_id", nullable = false)
	private Status from;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Status.class)
	@JoinColumn(name = "to_status_id", nullable = false)
	private Status to;

	public Status getFrom() {
		return from;
	}

	public void setFrom(Status from) {
		this.from = from;
	}

	public Status getTo() {
		return to;
	}

	public void setTo(Status to) {
		this.to = to;
	}

}

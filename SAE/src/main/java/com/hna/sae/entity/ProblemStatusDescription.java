package com.hna.sae.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sae_problem_status_desc")
public class ProblemStatusDescription extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ProblemStatus.class)
	@JoinColumn(name = "problem_status_id", nullable = false)
	private ProblemStatus problemStatus;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "active", nullable = false)
	private Boolean active;

	public ProblemStatus getProblemStatus() {
		return problemStatus;
	}

	public void setProblemStatus(ProblemStatus problemStatus) {
		this.problemStatus = problemStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}

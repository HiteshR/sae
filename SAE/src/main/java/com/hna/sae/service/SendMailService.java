package com.hna.sae.service;

import java.io.IOException;

import javax.mail.MessagingException;

import com.hna.sae.entity.User;

import freemarker.template.TemplateException;

public interface SendMailService {

	public void sendUserSignupMail(User user, String emailConfirmUrl,
			String token) throws MessagingException, IOException,
			TemplateException;

	public void sendForgotPasswordMail(User user, String redirectUrl,
			String token) throws MessagingException, IOException,
			TemplateException;

	public void sendSocialLoginMail(User user) throws MessagingException,
			IOException, TemplateException;

	public void sendMakeDelegateMail(User loginUser, User delegateUser)
			throws MessagingException, IOException, TemplateException;

}

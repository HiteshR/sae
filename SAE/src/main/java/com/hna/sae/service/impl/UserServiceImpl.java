package com.hna.sae.service.impl;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.hna.sae.common.Constant;
import com.hna.sae.config.SocialConfig.SocialProvider;
import com.hna.sae.dto.AccessTokenContainer;
import com.hna.sae.dto.AccountVerificationToken;
import com.hna.sae.dto.ChangePasswordDto;
import com.hna.sae.dto.EmailVerificationRequestDto;
import com.hna.sae.dto.ForgotPasswordDto;
import com.hna.sae.dto.OAuth2Request;
import com.hna.sae.dto.ResetPasswordDTO;
import com.hna.sae.dto.SignupUser;
import com.hna.sae.dto.UserLoginDto;
import com.hna.sae.dto.UserProfileDto;
import com.hna.sae.dto.ValidationErrorDTO;
import com.hna.sae.entity.Role;
import com.hna.sae.entity.SocialUser;
import com.hna.sae.entity.User;
import com.hna.sae.entity.UserRole;
import com.hna.sae.entity.VerificationToken;
import com.hna.sae.entity.VerificationToken.VerificationTokenType;
import com.hna.sae.exception.AlreadyVerifiedException;
import com.hna.sae.exception.AuthenticationException;
import com.hna.sae.exception.AuthorizationException;
import com.hna.sae.exception.FieldErrorException;
import com.hna.sae.exception.MessageException;
import com.hna.sae.exception.NotFoundException;
import com.hna.sae.exception.TokenHasExpiredException;
import com.hna.sae.exception.AlreadyVerifiedException.AlreadyVerifiedExceptionType;
import com.hna.sae.exception.MessageException.MessageExceptionErroCode;
import com.hna.sae.exception.NotFoundException.NotFound;
import com.hna.sae.repository.RoleRepository;
import com.hna.sae.repository.SocialUserRepository;
import com.hna.sae.repository.UserRepository;
import com.hna.sae.repository.UserRoleRepository;
import com.hna.sae.repository.VerificationTokenRepository;
import com.hna.sae.security.CustomUserDetail;
import com.hna.sae.security.InMemoryTokenStore;
import com.hna.sae.service.SendMailService;
import com.hna.sae.service.UserService;
import com.hna.sae.util.DateUtil;

import freemarker.template.TemplateException;

@Service(Constant.SERVICE_USER)
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserServiceImpl.class);

	@Resource(name = Constant.SERVICE_SEND_MAIL)
	private SendMailService sendMailService;

	@Resource
	private VerificationTokenRepository verificationTokenRepository;

	@Resource
	private UserRepository userRepository;

	@Resource
	private UserRoleRepository userRoleRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncode;

	@Resource
	private RoleRepository roleRepository;

	@Resource
	private SocialUserRepository socialUserRepository;

	@Autowired
	private ConnectionFactoryLocator connectionFactoryLocator;

	@Autowired
	private InMemoryTokenStore tokenStore;

	@Override
	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public UserDetails login(UserLoginDto loginDto) {
		logger.info("login Start");
		User objUser = this.userRepository.getUserByUserNameOrEmail(loginDto
				.getUserName());
		if (objUser != null) {
			if (objUser.getIsVerified()) {
				if (this.bCryptPasswordEncode.matches(loginDto.getPassword(),
						objUser.getPassword())) {
					List<String> userRoles = this.userRoleRepository
							.getUserRolesByUserNameOrEmail(loginDto
									.getUserName());
					// put all user role in list
					if (null != userRoles && userRoles.size() > 0) {
						ArrayList<GrantedAuthority> objAuthorities = new ArrayList<GrantedAuthority>();
						for (String role : userRoles) {
							SimpleGrantedAuthority objAuthority = new SimpleGrantedAuthority(
									role);
							objAuthorities.add(objAuthority);
						}
						return new CustomUserDetail(objUser.getUserName(),
								objUser.getPassword(), true, true, true, true,
								objAuthorities, objUser);
					} else {
						throw new NotFoundException(NotFound.UserNotFound);
					}
				} else {
					throw new AuthenticationException();
				}
			} else {
				throw new AuthenticationException(
						"Please confirm your account", "User not verified");
			}
		} else {
			throw new NotFoundException(NotFound.UserNotFound);
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public List<String> getUserRole(String userNameOrEmail) {
		logger.info("getUserRole");
		if (StringUtils.hasText(userNameOrEmail))
			return this.userRoleRepository
					.getUserRolesByUserNameOrEmail(userNameOrEmail);
		else
			return Collections.emptyList();
	}

	@Override
	public void logout(String token) {
		UserDetails user = this.tokenStore.readAccessToken(token);
		if (null != user) {
			this.tokenStore.removeToken(token);
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	// @Override
	// public User getUserByAccessToken(String token) {
	// return this.tokenStore.readAccessToken(token);
	// }

	@Override
	public AccessTokenContainer getAccessTokenContainer(UserDetails userDetails) {
		return this.tokenStore.generateAccessToken(userDetails);
	}

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = { Exception.class })
	public void forgotPassword(ForgotPasswordDto forgotPasswordModel)
			throws IOException, TemplateException, MessagingException {

		User objUser = this.userRepository
				.getUserByUserNameOrEmail(forgotPasswordModel
						.getUserNameOrEmail());

		// check user available in data base or not
		if (null != objUser) {
			if (objUser.getIsVerified()) {
				String verificationToken = this.verificationTokenRepository
						.getVerifactionTokenByEmail(objUser.getEmail(),
								VerificationTokenType.lostPassword);

				if (!StringUtils.hasText(verificationToken)) {
					// create verification token
					VerificationToken objVerificationToken = new VerificationToken(
							objUser, VerificationTokenType.lostPassword,
							Constant.LOST_PASSWORD_EMAIL_LINK_EXPIRE_TIME);
					objVerificationToken.setRedirectUrl(forgotPasswordModel
							.getRedirectUrl());
					objVerificationToken.setCreatedBy(objUser);
					objVerificationToken.setIsUsed(false);
					this.verificationTokenRepository.save(objVerificationToken);
					verificationToken = objVerificationToken.getToken();
				}

				// send mail
				this.sendMailService
						.sendForgotPasswordMail(objUser,
								forgotPasswordModel.getRedirectUrl(),
								verificationToken);
				// return responseData;
			} else {
				throw new AuthenticationException(
						"Please confirm your account", "User not verified");
			}
		} else {
			// if no data found send error message
			throw new NotFoundException(NotFound.UserNotFound);
		}
	}

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = { Exception.class })
	@Override
	public void resetPassword(ResetPasswordDTO resetPasswordDto) {
		VerificationToken objVerificationToken = this.verificationTokenRepository
				.getVerifactionTokenByToken(resetPasswordDto.getToken(),
						VerificationTokenType.lostPassword);
		if (objVerificationToken != null) {
			if (objVerificationToken.getIsUsed()) {
				throw new AlreadyVerifiedException(
						AlreadyVerifiedExceptionType.Token);
			} else {
				if (objVerificationToken.hasExpired()) {
					throw new TokenHasExpiredException();
				} else {
					User objUser = objVerificationToken.getUser();
					if (objUser != null) {
						objUser.setPassword(this.bCryptPasswordEncode
								.encode(resetPasswordDto.getRetypePassword()));
						objUser.setModifiedBy(objUser);
						objUser.setModifiedDate(new Date());
						// update
						this.userRepository.save(objUser);
						objVerificationToken.setIsUsed(true);
						// update
						this.verificationTokenRepository
								.save(objVerificationToken);
					} else {
						throw new NotFoundException(NotFound.TokenNotFound);
					}
				}
			}
		} else {
			throw new NotFoundException(NotFound.TokenNotFound);
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class })
	public void changePassword(ChangePasswordDto changePasswordDto,
			String userNameOrEmail) {
		User objUser = this.userRepository
				.getUserByUserNameOrEmail(userNameOrEmail);
		if (objUser != null) {
			if (this.bCryptPasswordEncode.matches(
					changePasswordDto.getCurrentPassword(),
					objUser.getPassword())) {
				if (!changePasswordDto.getCurrentPassword().equalsIgnoreCase(
						changePasswordDto.getRetypeNewPassword())) {
					objUser.setPassword(this.bCryptPasswordEncode
							.encode(changePasswordDto.getRetypeNewPassword()));
					objUser.setModifiedBy(objUser);
					objUser.setModifiedDate(DateUtil.getCurrentDate());
					// update
					this.userRepository.save(objUser);
				} else {
					ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();
					validationErrorDTO.addFieldError("newPassword",
							"Password cant be same as current.");
					throw new FieldErrorException(validationErrorDTO);
				}
			} else {
				ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();
				validationErrorDTO.addFieldError("currentPassword",
						"Password not match");
				throw new FieldErrorException(validationErrorDTO);
			}
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}

	}

	@Override
	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void signupUser(SignupUser signupUser) throws IOException,
			TemplateException, MessagingException {

		List<Role> objRoles = this.roleRepository.getDefaultRoles();

		if (objRoles != null && objRoles.size() > 0) {

			// save user master in database
			User objUser = new User();
			objUser.setIsVerified(false);
			objUser.setUserName(signupUser.getUserName());
			objUser.setEmail(signupUser.getEmail());
			objUser.setPassword(this.bCryptPasswordEncode.encode(signupUser
					.getPassword()));
			this.userRepository.saveAndFlush(objUser);

			// save user role in database
			for (Role role : objRoles) {
				UserRole objUserRole = new UserRole();
				objUserRole.setUser(objUser);
				objUserRole.setRole(role);
				objUserRole.setActive(true);
				objUserRole.setCreatedBy(objUser);
				this.userRoleRepository.save(objUserRole);
			}

			// create token and save in database
			VerificationToken objVerificationToken = new VerificationToken(
					objUser, VerificationTokenType.emailRegistration,
					Constant.REGISTER_EMAIL_LINK_EXPIRE_TIME);
			objVerificationToken.setRedirectUrl(signupUser.getRedirectURL());
			objVerificationToken.setCreatedBy(objUser);
			objVerificationToken.setIsUsed(false);
			this.verificationTokenRepository.save(objVerificationToken);

			// send registration mail
			this.sendMailService.sendUserSignupMail(objUser,
					signupUser.getRedirectURL(),
					objVerificationToken.getToken());
		} else {
			throw new NotFoundException(NotFound.DefaultRoleNotSet);
		}

	}

	@Override
	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void sendEmailVerificationToken(
			EmailVerificationRequestDto emailVerificationRequest)
			throws IOException, TemplateException, MessagingException {

		User objUser = this.userRepository
				.getUserByUserNameOrEmail(emailVerificationRequest.getEmail());
		if (objUser != null) {
			if (!objUser.getIsVerified()) {
				String objToken = this.verificationTokenRepository
						.getVerifactionTokenByEmail(
								emailVerificationRequest.getEmail(),
								VerificationTokenType.emailRegistration);
				if (!StringUtils.hasText(objToken)) {
					// create verification token
					VerificationToken objVerificationToken = new VerificationToken(
							objUser, VerificationTokenType.emailRegistration,
							Constant.REGISTER_EMAIL_LINK_EXPIRE_TIME);
					objVerificationToken
							.setRedirectUrl(emailVerificationRequest
									.getRedirectUrl());
					objVerificationToken.setCreatedBy(objUser);
					objVerificationToken.setIsUsed(false);
					this.verificationTokenRepository.save(objVerificationToken);
					objToken = objVerificationToken.getToken();
				}
				// send mail
				this.sendMailService.sendUserSignupMail(objUser,
						emailVerificationRequest.getRedirectUrl(), objToken);
			} else {
				throw new AlreadyVerifiedException(
						AlreadyVerifiedExceptionType.User);
			}
		} else {
			throw new NotFoundException(NotFound.UserNotFound);
		}

	}

	@Override
	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public void verifyUserAccount(
			AccountVerificationToken accountVerificationToken) {
		VerificationToken objVerificationToken = this.verificationTokenRepository
				.getVerifactionTokenByToken(
						accountVerificationToken.getToken(),
						VerificationTokenType.emailRegistration);
		if (objVerificationToken != null) {
			if (objVerificationToken.getIsUsed()) {
				throw new AlreadyVerifiedException(
						AlreadyVerifiedExceptionType.Token);
			} else if (objVerificationToken.hasExpired()) {
				throw new TokenHasExpiredException();
			} else {
				User objUser = objVerificationToken.getUser();
				objUser.setIsVerified(true);
				objUser.setModifiedDate(new Date());
				objUser.setModifiedBy(objUser);
				// update
				this.userRepository.save(objUser);
				objVerificationToken.setIsUsed(true);
				// update
				this.verificationTokenRepository.save(objVerificationToken);
			}
		} else {
			throw new NotFoundException(NotFound.TokenNotFound);
		}
	}

	@Override
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = { Exception.class })
	public UserDetails socialLogin(SocialProvider socialProvider,
			OAuth2Request auth2Request) throws MessagingException, IOException,
			TemplateException {
		List<SocialUser> objSocialUsers = null;
		ConnectionKey objConnectionKey = null;
		User objUser = null;
		List<Role> objRoles = null;
		UserRole objUserRole = null;
		SocialUser objSocialUser = null;
		ConnectionData objConnectionData = null;
		OAuth2ConnectionFactory<?> objConnectionFactory = null;
		Connection<?> objConnection = null;
		Boolean isFirstTime = Boolean.FALSE;
		ArrayList<GrantedAuthority> objAuthorities = new ArrayList<GrantedAuthority>();
		List<String> userRoles = null;
		try {
			objConnectionFactory = (OAuth2ConnectionFactory<?>) connectionFactoryLocator
					.getConnectionFactory(socialProvider.toString());
			objConnection = objConnectionFactory
					.createConnection(new AccessGrant(auth2Request
							.getAccessToken()));

			objConnectionKey = objConnection.getKey();
			objSocialUsers = this.socialUserRepository
					.findByProviderIdAndProviderUserId(
							objConnectionKey.getProviderId(),
							objConnectionKey.getProviderUserId());

			if (objSocialUsers != null && !objSocialUsers.isEmpty()) {
				for (SocialUser socialUser : objSocialUsers) {
					objUser = socialUser.getUser();
					userRoles = this.userRoleRepository
							.getUserRolesByUserNameOrEmail(objUser
									.getUserName());
				}
				if (!objUser.getIsVerified()) {
					objUser.setIsVerified(true);
					objUser.setModifiedBy(objUser);
					objUser.setModifiedDate(DateUtil.getCurrentDate());
					// update
					this.userRepository.save(objUser);
				}
			} else {
				UserProfile profile = objConnection.fetchUserProfile();
				if (profile != null && StringUtils.hasText(profile.getEmail())) {
					objUser = this.userRepository.getUserByEmail(profile
							.getEmail());

					if (objUser == null) {
						objRoles = this.roleRepository.getDefaultRoles();
						if (objRoles != null && objRoles.size() > 0) {
							userRoles = new ArrayList<String>();
							// save user master in database
							objUser = new User();
							objUser.setIsVerified(true);
							objUser.setUserName(profile.getEmail());
							objUser.setEmail(profile.getEmail());
							objUser.setPassword("");
							this.userRepository.save(objUser);

							// save user role in database
							for (Role role : objRoles) {
								userRoles.add(role.getRoleName());
								objUserRole = new UserRole();
								objUserRole.setUser(objUser);
								objUserRole.setRole(role);
								objUserRole.setActive(true);
								objUserRole.setCreatedBy(objUser);
								this.userRoleRepository.save(objUserRole);
							}

							// save data in user payment info
							isFirstTime = Boolean.TRUE;
						} else {
							throw new NotFoundException(
									NotFound.DefaultRoleNotSet);
						}
					} else {
						userRoles = this.userRoleRepository
								.getUserRolesByUserNameOrEmail(objUser
										.getUserName());
					}
					// save data in social user table if user logged in first
					// time
					// save social user data
					objConnectionData = objConnection.createData();
					objSocialUser = new SocialUser();
					objSocialUser.setAccessToken(objConnectionData
							.getAccessToken());
					objSocialUser.setCreatedBy(objUser);
					objSocialUser.setDisplayName(objConnectionData
							.getDisplayName());
					objSocialUser.setExpireTime(objConnectionData
							.getExpireTime());
					objSocialUser.setImageUrl(objConnectionData.getImageUrl());
					objSocialUser.setProfileUrl(objConnectionData
							.getProfileUrl());
					objSocialUser.setProviderId(objConnectionData
							.getProviderId());
					objSocialUser.setProviderUserId(objConnectionData
							.getProviderUserId());
					// TODO: currently only support 1 connection per
					// user per provider (rank = 1)
					objSocialUser.setRank(1);
					objSocialUser.setRefreshToken(objConnectionData
							.getRefreshToken());
					objSocialUser.setSecret(objConnectionData.getSecret());
					objSocialUser.setUser(objUser);
					this.socialUserRepository.save(objSocialUser);
				} else {
					throw new MessageException(
							MessageExceptionErroCode.NOTVALIDFBTOKEN);
				}
			}

			if (isFirstTime)
				this.sendMailService.sendSocialLoginMail(objUser);

		} finally {
			objSocialUsers = null;
			objConnectionKey = null;
			objRoles = null;
			objUserRole = null;
			objSocialUser = null;
			objConnectionData = null;
		}
		return new CustomUserDetail(objUser.getUserName(),
				objUser.getPassword(), true, true, true, true, objAuthorities,
				objUser);
	}

	@Transactional
	@Override
	public User getUserData(Principal principal) {
		User objUser = null;
		if (principal != null && principal.getName() != null) {
			objUser = this.userRepository.getUserByUserNameOrEmail(principal
					.getName());
		}
		return objUser;
	}

	// TODO need change
	@Override
	public UserProfileDto getUserProfile(String userNameOrEmail) {
		logger.info("getUserProfile");
		User objUser = this.userRepository
				.getUserByUserNameOrEmail(userNameOrEmail);
		if (objUser != null) {
			UserProfileDto objUserProfile = new UserProfileDto();
			objUserProfile.setUserName(objUser.getUserName());
			objUserProfile.setEmail(objUser.getEmail());
			objUserProfile.setPhoneNumber(objUser.getPhoneNumber());
			objUserProfile.setAddress(objUser.getAddress());
			objUserProfile.setCity(objUser.getCity());
			objUserProfile.setState(objUser.getState());
			objUserProfile.setZipcode(objUser.getZipcode());
			objUserProfile.setCountry(objUser.getCountry());
			return objUserProfile;
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

	@Override
	@Transactional
	public void updateUserProfile(String userNameOrEmail,
			com.hna.sae.dto.UserProfile userProfile) {
		logger.info("updateUserProfile start");
		// TODO boiler plated code
		// if (!loginUser.getEmail().equals(userProfile.getEmail())) {
		// String userName = this.userRepository
		// .getUserNameByEmail(userProfile.getEmail());
		// if (StringUtils.hasText(userName)
		// && !loginUser.getUserName().equals(userName)) {
		// ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();
		// validationErrorDTO
		// .addFieldError("email", "Email Already Exist");
		// throw new FieldErrorException(validationErrorDTO);
		// }
		// }
		// loginUser.setEmail(userProfile.getEmail());
		User objUser = this.userRepository
				.getUserByUserNameOrEmail(userNameOrEmail);
		if (objUser != null) {
			objUser.setAddress(userProfile.getAddress());
			objUser.setCity(userProfile.getCity());
			objUser.setState(userProfile.getState());
			objUser.setZipcode(userProfile.getZipcode());
			objUser.setCountry(userProfile.getCountry());
			objUser.setPhoneNumber(userProfile.getPhoneNumber());
			// update
			this.userRepository.save(objUser);
			// TODO send email if email address is changed
			// TODO boiler plated code
			logger.info("updateUserProfile old");
		} else {
			throw new AuthorizationException(Constant.ERROR_LOGIN_MESSAGE);
		}
	}

}

package com.hna.sae.service.impl;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.hna.sae.common.Constant;
import com.hna.sae.dto.UserLoginDto;
import com.hna.sae.exception.NotFoundException;
import com.hna.sae.exception.NotFoundException.NotFound;
import com.hna.sae.service.AdminService;
import com.hna.sae.service.SendMailService;

@Service(Constant.SERVICE_ADMIN)
public class AdminServiceImpl extends UserServiceImpl implements AdminService {

	@Resource(name = Constant.SERVICE_SEND_MAIL)
	private SendMailService sendMailService;

	private SimpleGrantedAuthority adminAuthority = new SimpleGrantedAuthority(
			"ADMIN");

	@Override
	@Transactional
	public UserDetails login(UserLoginDto loginDto) {
		UserDetails objUser = super.login(loginDto);
		if (objUser != null && !objUser.getAuthorities().isEmpty()
				&& objUser.getAuthorities().contains(adminAuthority)) {
			return objUser;
		} else {
			throw new NotFoundException(NotFound.NotAdminUser);
		}
	}

}

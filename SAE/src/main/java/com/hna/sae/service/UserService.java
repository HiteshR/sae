package com.hna.sae.service;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.security.core.userdetails.UserDetails;

import com.hna.sae.config.SocialConfig.SocialProvider;
import com.hna.sae.dto.AccessTokenContainer;
import com.hna.sae.dto.AccountVerificationToken;
import com.hna.sae.dto.ChangePasswordDto;
import com.hna.sae.dto.EmailVerificationRequestDto;
import com.hna.sae.dto.ForgotPasswordDto;
import com.hna.sae.dto.OAuth2Request;
import com.hna.sae.dto.ResetPasswordDTO;
import com.hna.sae.dto.SignupUser;
import com.hna.sae.dto.UserLoginDto;
import com.hna.sae.dto.UserProfile;
import com.hna.sae.dto.UserProfileDto;
import com.hna.sae.entity.User;

import freemarker.template.TemplateException;

public interface UserService {
	/**
	 * 
	 * @param loginDto
	 * @return
	 */
	public UserDetails login(UserLoginDto loginDto);

	/**
	 * 
	 * @param token
	 * @return
	 */
	public void logout(String token);

	/**
	 * 
	 * 
	 * @param forgotPasswordModel
	 * @return
	 * @throws IOException
	 * @throws TemplateException
	 * @throws MessagingException
	 */
	public void forgotPassword(ForgotPasswordDto forgotPasswordModel)
			throws IOException, TemplateException, MessagingException;

	/**
	 * 
	 * @param resetPasswordDto
	 * @return
	 */
	public void resetPassword(ResetPasswordDTO resetPasswordDto);

	/**
	 * signupUser
	 * 
	 * @param signupUser
	 * @return
	 * @throws MessagingException
	 * @throws TemplateException
	 * @throws IOException
	 * @throws DefaultRoleNotSetException
	 */
	public void signupUser(SignupUser signupUser) throws IOException,
			TemplateException, MessagingException;

	/**
	 * sendEmailVerificationToken send email verification token to user
	 * 
	 * @param emailVerificationRequest
	 * @return
	 * @throws IOException
	 * @throws TemplateException
	 * @throws MessagingException
	 */
	public void sendEmailVerificationToken(
			EmailVerificationRequestDto emailVerificationRequest)
			throws IOException, TemplateException, MessagingException;

	/**
	 * verifyUserAccount verify user send succes or fail message
	 * 
	 * @param accountVerificationToken
	 * @return
	 */
	public void verifyUserAccount(
			AccountVerificationToken accountVerificationToken);

	/**
	 * socialLogin logged in into system using access token
	 * 
	 * @param socialProvider
	 * @param auth2Request
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 * @throws TemplateException
	 */
	public UserDetails socialLogin(SocialProvider socialProvider,
			OAuth2Request auth2Request) throws MessagingException, IOException,
			TemplateException;

	/**
	 * 
	 * @param principal
	 * @return
	 */
	public User getUserData(Principal principal);

	public void updateUserProfile(String userNameOrEmail,
			UserProfile userProfile);

	// TODO changed
	public UserProfileDto getUserProfile(String userNameOrEmail);

	public AccessTokenContainer getAccessTokenContainer(UserDetails userDetails);

	public List<String> getUserRole(String userNameOrEmail);

	public void changePassword(ChangePasswordDto changePasswordDto,
			String userNameOrEmail);

	// public User getUserByAccessToken(String token);

}

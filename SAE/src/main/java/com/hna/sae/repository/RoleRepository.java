package com.hna.sae.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.hna.sae.entity.Role;

public interface RoleRepository extends BaseRepository<Role> {

	@Query(value = "SELECT role FROM Role AS role WHERE role.isDefault=true AND role.active=true")
	public List<Role> getDefaultRoles();
}
